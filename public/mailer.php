<?php

// Some basic endpoint protection
if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) die('2');
if ($_SERVER['PHP_AUTH_USER'] !== 'admin' || $_SERVER['PHP_AUTH_PW'] !== 'admin') die('14');
$raw = file_get_contents('php://input');

// Try to parse incoming json
$json = json_decode($raw);
if (json_last_error() !== JSON_ERROR_NONE) {
    die('21');
}

// Send the mail
if (mail(implode(', ', $json->recipients), 'Lunch Dating: You matched with ' . count($json->recipients) . ' other people!', $json->body)) {
    echo 'OK' . PHP_EOL;
} else {
    echo 'FAILED' . PHP_EOL;
}