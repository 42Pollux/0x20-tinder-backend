<?php
/**
 * @author p314406 <Michael.Buelow@fashion-digital.de>
 * @since 2020-01-25
 */

namespace App\Validator;

/**
 * Class ViolationHelper
 * @package App\Validator
 */
class ViolationHelper
{
    /**
     * @param $violations
     * @return array
     */
    public static function mapViolationsToArray($violations): array
    {
        $errors = array();
        foreach ($violations as $violation) {
            $errors[] = 'Field \'' . $violation->getPropertyPath() . '\': ' . $violation->getmessage();
        }

        return $errors;
    }
}
