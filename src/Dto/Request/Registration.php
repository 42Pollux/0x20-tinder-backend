<?php
/**
 * @author p314406 <Michael.Buelow@fashion-digital.de>
 * @since 2020-01-25
 */

namespace App\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;

class Registration
{
    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Email()
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $date;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @var string
     */
    public $location;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(string $location): void
    {
        $this->location = $location;
    }
}