<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TblUserRepository")
 */
class TblUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TblAppointment", mappedBy="tbl_user")
     */
    private $tblAppointments;

    public function __construct()
    {
        $this->tblAppointments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|TblAppointment[]
     */
    public function getTblAppointments(): Collection
    {
        return $this->tblAppointments;
    }

    public function addTblAppointment(TblAppointment $tblAppointment): self
    {
        if (!$this->tblAppointments->contains($tblAppointment)) {
            $this->tblAppointments[] = $tblAppointment;
            $tblAppointment->setTblUser($this);
        }

        return $this;
    }

    public function removeTblAppointment(TblAppointment $tblAppointment): self
    {
        if ($this->tblAppointments->contains($tblAppointment)) {
            $this->tblAppointments->removeElement($tblAppointment);
            // set the owning side to null (unless already changed)
            if ($tblAppointment->getTblUser() === $this) {
                $tblAppointment->setTblUser(null);
            }
        }

        return $this;
    }
}
