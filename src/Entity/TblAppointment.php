<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TblAppointmentRepository")
 */
class TblAppointment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TblUser", inversedBy="tblAppointments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tbl_user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAt(): ?string
    {
        return $this->at->format('Y-m-d H:i:s');
    }

    public function setAt(string $at): self
    {
        $this->at = new \DateTime($at);

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getTblUser(): ?TblUser
    {
        return $this->tbl_user;
    }

    public function setTblUser(?TblUser $tbl_user): self
    {
        $this->tbl_user = $tbl_user;

        return $this;
    }
}
