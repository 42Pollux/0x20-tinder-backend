<?php
/**
 * @author p314406 <Michael.Buelow@fashion-digital.de>
 * @since 2019-07-22
 */

namespace App\EventSubscriber;


use App\Exception\ValidationException;
use App\Serializer\Serializer;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ExceptionSerializerSubscriber
 * @package App\EventSubscriber
 */
class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ExceptionSubscriber constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::EXCEPTION => array(
                'onExceptionEvent'
            )
        );
    }

    /**
     * @param ExceptionEvent $event
     */
    public function onExceptionEvent(ExceptionEvent $event) : void
    {
        $exception = $event->getThrowable();
        $exceptionCode = $exception->getCode();

        if ($exceptionCode <= 0 || $exceptionCode >= 600) {
            $exceptionCode = 500;
        }

        // Fehler sammeln
        $errors = array($exception->getMessage());
        if ($exception instanceof ValidationException) {
            $errors = $exception->getViolations();
            $exceptionCode = 400;
        }

        // Alle Fehler vom ganzen Stack sammeln
        $exception = $exception->getPrevious();
        while ($exception instanceof \Exception) {
            $errors = array_merge(array($exception->getMessage()), $errors);
            $exception = $exception->getPrevious();
        }#

        // Antwort serialisieren
        $serializer = new Serializer();
        $root = [];
        $meta = [];
        $data = [];
        $meta['code'] = $exceptionCode;
        $meta['errors'] = $errors;
        $root['meta'] = $meta;
        $root['data'] = $data;



        // Symfony Reponse erstellen
        $response = new Response(json_encode($root), $exceptionCode, array(
            'Content-Type' => 'application/json'
        ));

        $event->setResponse($response);

        return;
    }
}
