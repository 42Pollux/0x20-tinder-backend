<?php

namespace App\Controller;

use App\Dto\Request\Registration;
use App\Entity\TblAppointment;
use App\Entity\TblUser;
use App\Exception\ValidationException;
use App\Repository\TblAppointmentRepository;
use App\Repository\TblUserRepository;
use App\Serializer\Serializer;
use App\Validator\ViolationHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationController extends AbstractController
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var TblUserRepository
     */
    private $userRepo;

    /**
     * @var TblAppointmentRepository
     */
    private $appointmentRepo;

    /**
     * RegistrationController constructor.
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        $this->validator = $validator;
        $this->userRepo = $entityManager->getRepository(TblUser::class);
        $this->appointmentRepo = $entityManager->getRepository(TblAppointment::class);
    }

    /**
     * @Route("/register", name="app_register", methods={"POST"})
     * @throws \Exception
     */
    public function register(Request $request)
    {
        // Deserialisieren der Payload
        $serializer = new Serializer();
        $regDto = $serializer->deserialize(
            $request->getContent(),
            Registration::class
        );

        // Validieren der Auftragsdaten
        $violations = $this->validator->validate($regDto);
        if (count($violations) > 0) {
            throw new ValidationException(ViolationHelper::mapViolationsToArray($violations));
        }

        $user = $this->userRepo->createUserIfNotExists($regDto->getEmail());
        $this->appointmentRepo->createAppointment($user, $regDto->getDate(), $regDto->getLocation());

        return new Response(null, 201);
    }

    /**
     * @Route("/unregister", name="app_unregister", methods={"POST"})
     * @throws \Exception
     */
    public function unregister()
    {
        // not yet implemented

        throw new \Exception('not yet supported');
    }
}
