<?php

namespace App\Command;

use App\Entity\TblAppointment;
use App\Repository\TblAppointmentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;

class CronjobCommand extends Command
{
    protected static $defaultName = 'app:cronjob';

    private const MAX_GROUP_SIZE = 5;
    private const MIN_GROUP_SIZE = 2;

    /**
     * @var TblAppointmentRepository
     */
    private $repoAppointment;

    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->repoAppointment = $entityManager->getRepository(TblAppointment::class);
    }

    /**
     * @param InputInterface $in
     * @param OutputInterface $out
     * @return int
     * @throws \Exception
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $in, OutputInterface $out): int
    {
        // Get all registrations/attendees for today
        $registrations = $this->repoAppointment->getRegistrationsForToday();
        $attendee_count = count($registrations);
        echo 'Unique attendees/registrations for today: ' . $attendee_count . PHP_EOL;

        // Abort if not enough attendees
        if ($attendee_count < self::MIN_GROUP_SIZE) {
            echo 'Not enough attendees to form groups. Aborting...' . PHP_EOL;
            return 0;
        }

        // Calculating group sizes
        echo 'Calculating group sizes...' . PHP_EOL;
        $gcd = $this->gcd($attendee_count, self::MIN_GROUP_SIZE, self::MAX_GROUP_SIZE);
        $gcd_grp_size = $gcd[0];
        $gcd_remaining_grp_size = $gcd[1];

        // Randomly shuffle all attendees
        echo 'Randomly shuffling all attendees...' . PHP_EOL;
        $emails = $this->shuffle($registrations, $attendee_count * 4);

        // Creating subgroups
        echo 'Creating subgroups...' . PHP_EOL;
        $groups = [];
        $normal_grp_count = (int) ($attendee_count / $gcd_grp_size);
        for ($i = 0; $i < $normal_grp_count; $i++) {
            $subgroup = [];
            for ($j = 0; $j < $gcd_grp_size; $j++) {
                $subgroup[] = $emails[$i * $gcd_grp_size + $j];
            }
            $groups[] = $subgroup;
        }

        // If the groups are uneven we also grab the remaining guys and create another subgroup
        if ($gcd_remaining_grp_size > 0) {
            $remaining_group = [];
            for ($i = ($attendee_count - $gcd_remaining_grp_size); $i < $attendee_count; $i++) {
                $remaining_group[] = $emails[$i];
            }
            $groups[] = $remaining_group;
        }

        // Some line breaks so everything looks nice and tidy
        echo PHP_EOL . 'Groups: ' . PHP_EOL;

        // Print out the groups
        for ($i = 0; $i < count($groups); $i++) {
            echo '  ' . ($i+1) . ': group' . PHP_EOL;
            foreach ($groups[$i] as $g) {
                echo '    -> ' . $g->getTblUser()->getEmail() . ' (' . $g->getAt() . ')' . PHP_EOL;
            }
        }

        // Define json object structure for http request
        $json = new \stdClass();
        $json->recipients = [];
        $json->body = '';

        // Send emails
        $http = HttpClient::create();
        foreach ($groups as $subgroup) {
            $json->recipients = [];
            $json->body = 'Hello there!' . PHP_EOL . PHP_EOL . 'You\'ve been matched with these lovely fellas: ' . PHP_EOL;
            foreach ($subgroup as $recipient) {
                $json->body .= $recipient->getTblUser()->getEmail() . PHP_EOL;
                $json->recipients[] = $recipient->getTblUser()->getEmail();
            }
            $json->body .= PHP_EOL . 'Your lunch date will be at {location} at around {time}.' . PHP_EOL;


            $http->request(
                'POST',
                'http://janwennmann.bplaced.net/tinder/index.php',
                array(
                    'auth_basic' => ['admin', 'admin'],
                    'timeout' => 10,
                    'body' => json_encode($json)
                )
            );
        }

        return 0;
    }

    /**
     * Calculates the greatest common divisor of $num with numbers
     * in between $min and $max. In case there is none, returns the
     * one with the biggest remainder.
     * Result for gcd: [gcd, 0]
     * Result for no gcd: [gcd of biggest remainder, biggest remainder]
     *
     * @param $num
     * @return array
     */
    public function gcd($num, $min, $max)
    {
        $pref_grp_size = 0;
        $best_mod_size = 0;
        for ($i = $max; $i >= $min; $i--) {
            if ($num%$i === 0) {
                return [$i, 0];
            }
            if ($num%$i > $best_mod_size) {
                $pref_grp_size = $i;
                $best_mod_size = $num%$i;
            }
        }

        return [$pref_grp_size, $best_mod_size];
    }

    /**
     * Shuffles an array $times times.
     *
     * @param $array array Array to shuffle.
     * @param $times int Amount of times to shuffle.
     * @return array
     */
    public function shuffle($array, $times)
    {
        $max_size = count($array);
        for ($i = 0; $i < $times; $i++) {
            $a = rand(0, $max_size - 1);
            $b = rand(0, $max_size - 1);
            $tmp = $array[$a];
            $array[$a] = $array[$b];
            $array[$b] = $tmp;
        }

        return $array;
    }
}
