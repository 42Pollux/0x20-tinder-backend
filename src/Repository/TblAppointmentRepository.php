<?php

namespace App\Repository;

use App\Entity\TblAppointment;
use App\Entity\TblUser;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TblAppointment|null find($id, $lockMode = null, $lockVersion = null)
 * @method TblAppointment|null findOneBy(array $criteria, array $orderBy = null)
 * @method TblAppointment[]    findAll()
 * @method TblAppointment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TblAppointmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblAppointment::class);
    }

    /**
     * @param TblUser $user
     * @param string $at
     * @param string $location
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createAppointment(TblUser $user, string $at, string $location)
    {
        $newAppointment = new TblAppointment();
        $newAppointment->setTblUser($user);
        $newAppointment->setAt($at);
        $newAppointment->setLocation($location);

        $this->getEntityManager()->persist($newAppointment);
        $this->getEntityManager()->flush();
    }

    /**
     * Get all registrations for today (within +-12h from now)
     *
     * @return array
     * @throws \Exception
     */
    public function getRegistrationsForToday()
    {
        // Get all registrations from the db
        $registration = $this->findAll();

        $today = new DateTime('now');
        $possible_registrations = [];
        foreach ($registration as $reg) {

            // Check if within 12h boundary
            $date = new DateTime($reg->getAt());
            $diff = abs($date->diff($today)->h + $date->diff($today)->d*24 + $date->diff($today)->m*30*24);
            if ($diff <= 12) {
                $possible_registrations[$reg->getTblUser()->getId()] = $reg;
            }
        }

        return array_values($possible_registrations);
    }
}
