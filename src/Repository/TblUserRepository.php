<?php

namespace App\Repository;

use App\Entity\TblUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TblUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method TblUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method TblUser[]    findAll()
 * @method TblUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TblUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblUser::class);
    }

    /**
     * @param string $email
     * @throws \Doctrine\ORM\ORMException
     */
    public function createUserIfNotExists(string $email)
    {
        $users = $this->findBy([
            'email' => $email
        ]);

        if (!empty($users)) {
            // User already exists
            return $users[0];
        }

        // Create new user
        $newUser = new TblUser();
        $newUser->setEmail($email);
        $this->getEntityManager()->persist($newUser);
        $this->getEntityManager()->flush();

        return $newUser;
    }

}
