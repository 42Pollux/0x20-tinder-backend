<?php
/**
 * @author p314406 <Michael.Buelow@fashion-digital.de>
 * @since 2020-01-25
 */

namespace App\Exception;


use Throwable;

/**
 * Class ValidationException
 * @package App\Exception
 */
class ValidationException extends \Exception
{
    /**
     * @var array|string
     */
    private $violations = array();

    /**
     * ValidationException constructor.
     * @param string $violations
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($violations = [], $code = 400, Throwable $previous = null)
    {
        $this->violations = $violations;
        parent::__construct(implode("\n", $violations), $code, $previous);
    }

    /**
     * @return array
     */
    public function getViolations(): array
    {
        return $this->violations;
    }
}
